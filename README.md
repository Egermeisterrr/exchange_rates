This is currency exchange rates application. It has list of popular currencies and you can choose the currency you want to convert to others. Besides, you can sort the list of currencies and add currencies to favorites.

Using api: https://exchangeratesapi.io/

Stack: Clean Architecture, MVVM, Hilt, Retrofit, Room, Coroutines

![image.png](./image.png)

![image-1.png](./image-1.png)

![image-2.png](./image-2.png)

![image-3.png](./image-3.png)
